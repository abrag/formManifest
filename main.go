package main

import (
	"github.com/gin-gonic/gin"
)

type validationInputData map[string]any

func setupFormsAndComponents() repository {
	r := repository{}
	r.build()
	return r
}

func setupRouter(r repository) *gin.Engine {
	router := gin.Default()
	fc := formControler(r)
	cc := componentControler{
		components: r.components,
	}
	router.GET("/form/:name", fc.GetByName)
	router.POST("/form/:name", fc.ValidateByName)
	router.GET("/values/:source", cc.GetValuesBySource)

	return router
}

func main() {
	router := setupRouter(setupFormsAndComponents())
	router.Run()
}
