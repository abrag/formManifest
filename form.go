package main

import "gitlab.com/abrag/formManifest/component"

type formManifest struct {
	name       string
	Components []component.ComponentManifest `json:"components"`
}

func (fm *formManifest) validate(inputValue validationInputData) bool {
	ok := false
	leftToValidate := len(inputValue)
	for _, c := range fm.Components {
		componentName := c.Source
		_, ok = inputValue[componentName]
		leftToValidate = leftToValidate - 1
	}

	return ok && leftToValidate == 0
}
