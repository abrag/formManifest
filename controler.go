package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/abrag/formManifest/component"
)

type formControler struct {
	forms      map[string]formManifest
	components map[string]component.Component
}

type componentControler struct {
	components map[string]component.Component
}

func (fc formControler) ValidateByName(c *gin.Context) {
	var inData validationInputData

	if err := c.BindJSON(&inData); err != nil {
		c.IndentedJSON(http.StatusBadRequest, "Bad Request")
		return
	}

	validComponents := true
	form, validForm := fc.forms[c.Param("name")]
	if validForm {
		if form.validate(inData) {
			for k, val := range inData {
				validComponents = validComponents && fc.components[k].Validate(val)
				if !validComponents {
					c.IndentedJSON(http.StatusBadRequest, "Provided values are not acceptable")
					break
				}
			}
			c.IndentedJSON(http.StatusOK, "Success")
		} else {
			c.IndentedJSON(http.StatusBadRequest, "Provided values are not acceptable")
		}
	} else {
		c.IndentedJSON(http.StatusNotFound, "Not Found")
	}
}

func (fc formControler) GetByName(c *gin.Context) {
	val, ok := fc.forms[c.Param("name")]
	if ok {
		c.IndentedJSON(http.StatusOK, val)
	} else {
		c.IndentedJSON(http.StatusNotFound, "Not Found")
	}
}

func (cc componentControler) GetValuesBySource(c *gin.Context) {
	val, ok := cc.components[c.Param("source")]
	if ok {
		c.IndentedJSON(http.StatusOK, val.GetValues())
	} else {
		c.IndentedJSON(http.StatusNotFound, "Not Found")
	}
}
