package component

import (
	"log"
)

type FreeTextComponent struct {
	Manifest       ComponentManifest
	ValueGetter    componentStringValueGetter
	ValueValidator componentStringValueValidator
}

func (c FreeTextComponent) GetValues() componentValues {
	return componentValues{Values: c.ValueGetter()}
}

func (c FreeTextComponent) Validate(value any) bool {
	v, ok := value.(string)

	if !ok {
		log.Println("assert failed")
		return false
	}

	return c.ValueValidator(c.ValueGetter(), v)
}

func (c FreeTextComponent) GetComponentManifest() ComponentManifest {
	return c.Manifest
}

func FreeTextValueGetter() []string {
	return []string{}
}

func FreeTextValueValidator(_ []string, _ string) bool {
	return true
}
