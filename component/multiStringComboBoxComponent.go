package component

import (
	"log"

	"golang.org/x/exp/slices"
)

type MultiStringComboBoxComponent struct {
	Manifest       ComponentManifest
	ValueGetter    componentStringValueGetter
	ValueValidator componentMultiStringValueValidator
}

func (c MultiStringComboBoxComponent) GetValues() componentValues {
	return componentValues{Values: c.ValueGetter()}
}

func (c MultiStringComboBoxComponent) Validate(value any) bool {
	originalInput, ok := value.([]any)

	if !ok {
		log.Println("assert failed - not an Array/Slice")
		return false
	}

	parsedInputs := make([]string, 0, len(originalInput))
	var parsedValue string
	for _, i := range originalInput {
		parsedValue, ok = i.(string)
		if !ok {
			log.Println("assert failed - not a string")
			return false
		}
		parsedInputs = append(parsedInputs, parsedValue)
	}

	return c.ValueValidator(c.ValueGetter(), parsedInputs)
}

func (c MultiStringComboBoxComponent) GetComponentManifest() ComponentManifest {
	return c.Manifest
}

func MultiStringComboBoxValidator(values, subset []string) bool {
	ok := true
	for _, v := range subset {
		if !slices.Contains(values, v) {
			ok = false
		}
	}
	return ok
}
