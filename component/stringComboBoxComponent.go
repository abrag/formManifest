package component

import (
	"log"

	"golang.org/x/exp/slices"
)

type StringComboBoxComponent struct {
	Manifest       ComponentManifest
	ValueGetter    componentStringValueGetter
	ValueValidator componentStringValueValidator
}

func (c StringComboBoxComponent) GetValues() componentValues {
	return componentValues{Values: c.ValueGetter()}
}

func (c StringComboBoxComponent) Validate(value any) bool {
	v, ok := value.(string)

	if !ok {
		log.Println("assert failed")
		return false
	}
	return c.ValueValidator(c.ValueGetter(), v)
}

func (c StringComboBoxComponent) GetComponentManifest() ComponentManifest {
	return c.Manifest
}

func StringComboBoxValidator(values []string, s string) bool {
	return slices.Contains(values, s)
}
