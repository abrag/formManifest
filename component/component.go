package component

// type componentValueGetter func() ([]byte, error)
type componentStringValueGetter func() []string
type componentIntValueGetter func() []int

type componentStringValueValidator func([]string, string) bool
type componentMultiStringValueValidator func([]string, []string) bool
type componentIntValueValidator func([]int, int) bool

type componentValues struct {
	Values any `json:"values"`
}

type ComponentManifest struct {
	Source        string `json:"source"`
	Label         string `json:"label"`
	ComponentType string `json:"component_type"`
	Placeholder   string `json:"placeholder"`
	InputType     string `json:"input_type"`
}

type Component interface {
	GetComponentManifest() ComponentManifest
	Validate(value any) bool
	GetValues() componentValues
}
