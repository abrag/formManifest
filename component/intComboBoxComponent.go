package component

import (
	"log"

	"golang.org/x/exp/slices"
)

type IntComboBoxComponent struct {
	Manifest       ComponentManifest
	ValueGetter    componentIntValueGetter
	ValueValidator componentIntValueValidator
}

func (c IntComboBoxComponent) GetValues() componentValues {
	return componentValues{Values: c.ValueGetter()}
}

func isIntegral(val float64) bool {
	return val == float64(int(val))
}

func (c IntComboBoxComponent) Validate(value any) bool {
	v, ok := value.(float64)

	if !ok {
		log.Println("assert failed - not integer ")
		return false
	}
	if !isIntegral(v) {
		log.Println("assert failed - not integer")
		return false
	}

	return c.ValueValidator(c.ValueGetter(), int(v))
}

func (c IntComboBoxComponent) GetComponentManifest() ComponentManifest {
	return c.Manifest
}

func IntComboBoxValidator(values []int, i int) bool {
	return slices.Contains(values, i)
}
