package main

import "gitlab.com/abrag/formManifest/component"

type repository struct {
	forms      map[string]formManifest
	components map[string]component.Component
}

func (b *repository) build() {
	b.components = map[string]component.Component{
		"databaseInstances": component.StringComboBoxComponent{
			Manifest: component.ComponentManifest{
				Source:        "databaseInstances",
				Label:         "Database Instances",
				ComponentType: "ComboBox",
				Placeholder:   "choose database",
				InputType:     "String",
			},
			ValueGetter: func() []string {
				return []string{"Prod", "ReadReplica", "Stage"}
			},
			ValueValidator: component.StringComboBoxValidator,
		},
		"databaseRoles": component.StringComboBoxComponent{
			Manifest: component.ComponentManifest{
				Source:        "databaseRoles",
				Label:         "Database Role",
				ComponentType: "ComboBox",
				Placeholder:   "choose a role",
				InputType:     "String",
			},
			ValueGetter: func() []string {
				return []string{"Read Only", "Read write", "Full Admin"}
			},
			ValueValidator: component.StringComboBoxValidator,
		},
		"reason": component.FreeTextComponent{
			Manifest: component.ComponentManifest{
				Source:        "reason",
				Label:         "Reason",
				ComponentType: "TextField",
				Placeholder:   "Enter your Reason for access",
				InputType:     "String",
			},
			ValueGetter:    component.FreeTextValueGetter,
			ValueValidator: component.FreeTextValueValidator,
		},
		"urgency": component.IntComboBoxComponent{
			Manifest: component.ComponentManifest{
				Source:        "urgency",
				Label:         "Urgency Level",
				ComponentType: "ComboBox",
				Placeholder:   "choose Urgency",
				InputType:     "Int",
			},
			ValueGetter: func() []int {
				return []int{1, 2, 3}
			},
			ValueValidator: component.IntComboBoxValidator,
		},
		"user": component.StringComboBoxComponent{
			Manifest: component.ComponentManifest{
				Source:        "user",
				Label:         "User",
				ComponentType: "ComboBox",
				Placeholder:   "choose a user",
				InputType:     "Email",
			},
			ValueGetter: func() []string {
				return []string{"a@a.io", "b@b.io", "c@c.io"}
			},
			ValueValidator: component.StringComboBoxValidator,
		},
		"accounts": component.MultiStringComboBoxComponent{
			Manifest: component.ComponentManifest{
				Source:        "accounts",
				Label:         "Accounts",
				ComponentType: "MultiComboBox",
				Placeholder:   "select all requested accounts",
				InputType:     "[]String",
			},
			ValueGetter: func() []string {
				return []string{"Prod-A", "PCI-A", "Playground-A"}
			},
			ValueValidator: component.MultiStringComboBoxValidator,
		},
	}
	b.forms = map[string]formManifest{
		"databaseAccess": {
			name: "databaseAccess",
			Components: []component.ComponentManifest{
				b.components["reason"].GetComponentManifest(),
				b.components["databaseInstances"].GetComponentManifest(),
				b.components["databaseRoles"].GetComponentManifest(),
			},
		},
		"cloudAccess": {
			name: "cloudAccess",
			Components: []component.ComponentManifest{
				b.components["reason"].GetComponentManifest(),
				b.components["accounts"].GetComponentManifest(),
				b.components["user"].GetComponentManifest(),
			},
		},
		"test": {
			name: "test",
			Components: []component.ComponentManifest{
				// compontentsMap["accounts"].GetComponentManifest(),
				b.components["user"].GetComponentManifest(),
				b.components["urgency"].GetComponentManifest(),
			},
		},
	}
}
