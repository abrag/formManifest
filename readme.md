# Axiom FormManifest
Some explanation about my code:
1) each concrete Component is a composite of :
	* a component metadata (source, label,...) we will call it ComponentManifest
	* a value getter function
	* an input validator function
2) a Form is a list of ComponentManifests
3) Component Interface - is how to interact with a component, each concrete Component should implement the API
	* GetComponentManifest() - retrieve the component metadata 
	* Validate(value any) bool - parse the input from any to the specific type and forward the validation to the validator function defined in (1)
	* GetValues() componentValues - forward the get values to the getter defined in (1)


## Run via CLI:
 `go  mod  download`

 `go run .`
 
running test
 to run tests `go test`

## Run via docker:
 `docker build . -t form_manifest`
 `docker run -dp 3000:3000 -t form_manifest`

## ToDo:

* Align folder and package structure to "go gin" best practices
* Better error handling
