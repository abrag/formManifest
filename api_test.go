package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func reader(inputData map[string]any) *bytes.Buffer {
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(inputData)
	return b
}

func TestFormRouteCloudAccessValidationFail(t *testing.T) {
	// we can pass different components and forms for testing
	router := setupRouter(setupFormsAndComponents())

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/form/cloudAccess", reader(map[string]any{
		"accounts": []string{"PCI-A", "Prod-A"},
		"user":     "a@a.io",
	}))
	router.ServeHTTP(w, req)
	assert.Equal(t, 400, w.Code)
}

func TestFormRouteCloudAccessValidationOk(t *testing.T) {
	router := setupRouter(setupFormsAndComponents())

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/form/cloudAccess", reader(map[string]any{
		"reason":   "adas",
		"accounts": []string{"PCI-A", "Prod-A"},
		"user":     "a@a.io",
	}))

	router.ServeHTTP(w, req)
	assert.Equal(t, 200, w.Code)
}

func TestFormRouteDatabaseAccessValidationFail(t *testing.T) {
	// we can pass different components and forms for testing
	router := setupRouter(setupFormsAndComponents())

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/form/databaseAccess", reader(map[string]any{
		"reason":            "need access to prod",
		"databaseInstances": "PROD_PCI_X",
		"databaseRoles":     "Full Admin",
	}))
	router.ServeHTTP(w, req)
	assert.Equal(t, 400, w.Code)
}

func TestFormRouteDatabaseAccessValidationOk(t *testing.T) {
	router := setupRouter(setupFormsAndComponents())

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/form/databaseAccess", reader(map[string]any{
		"reason":            "need access to prod",
		"databaseInstances": "ReadReplica",
		"databaseRoles":     "Full Admin",
	}))
	router.ServeHTTP(w, req)
	assert.Equal(t, 200, w.Code)
}

func TestComponentRouteEmptyValueForReasonOK(t *testing.T) {
	router := setupRouter(setupFormsAndComponents())

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/values/reason", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "{\n    \"values\": []\n}", w.Body.String())
}
