FROM golang:alpine

WORKDIR /app

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .

# RUN go install
RUN go build .

ENV PORT 3000
ENV GIN_MODE release
EXPOSE 3000

CMD [ "./formManifest" ]